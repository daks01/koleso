function searchShowHide(search_field){
    if($(window).width() <= 1000){
        if(search_field.is(':visible')){
            search_field.hide().parents('li').addClass('search_min');
        }
    }else{
        if(!search_field.is(':visible')){
            search_field.show().parents('li').removeClass('search_min');
        }
    }
}

$(document).ready(function(){

    searchShowHide($('.search input[type=text]'));

    jQuery('.slide-nav').click(function(){
        if(jQuery(this).hasClass('active')){
            return false;
        }
        var self = jQuery(this);
        var target = jQuery('.' + self.data('target'));
        var targetW = target.width();
        var baloon = target.find('.baloon');
        baloon.hide();
        target.siblings().stop().animate({'left':'-'+targetW}, 500, function() {
            self.fadeIn(1000).addClass('active').siblings().removeClass('active');
            target.siblings().removeClass('active').css('left','0');
            target.addClass('active');
            baloon.show();
        });
    });
    
    jQuery('.menu .active').click(function(){
        return false;
    });


});

$(window).resize(function(){

    searchShowHide($('.search input[type=text]'));

});